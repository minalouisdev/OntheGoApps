import {
  IonContent,
  IonHeader,
  IonItem,
  IonList,
  IonMenu,
  IonMenuToggle,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import { useDispatch } from "react-redux";
import {
  actionSettingsMobileOpenApp,
  actionSettingsSetMobileHeaderTitle,
} from "../../redux/settingsReducer";
const MainMenu = () => {
  const dispatch = useDispatch(null);
  return (
    <IonMenu contentId="MainPage">
      <IonHeader>
        <IonToolbar mode="ios">
          <IonTitle>Menu</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          <IonMenuToggle>
            <IonItem
              button
              onClick={() => {
                dispatch(actionSettingsSetMobileHeaderTitle("Budget"));
                dispatch(actionSettingsMobileOpenApp("Budget"));
              }}
            >
              Budget
            </IonItem>
          </IonMenuToggle>
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default MainMenu;
