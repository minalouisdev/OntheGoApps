import { IonButton } from "@ionic/react";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  actionSettingsMobileOpenApp,
  actionSettingsSetMobileHeaderTitle,
} from "../redux/settingsReducer";
import { actionBudgetGoBack } from "../redux/budgetReducer";
const BudgetHeaderBtn = () => {
  const budgetHeaderBtn = useSelector(
    (state) => state.budgetReducer.budgetHeaderBtn
  );
  const dispatch = useDispatch(null);
  const handleBudgetHeaderBtnContent = () => {
    switch (budgetHeaderBtn) {
      case "Close":
        return (
          <IonButton
            onClick={() => {
              dispatch(actionSettingsSetMobileHeaderTitle("OnTheGoApps"));
              dispatch(actionSettingsMobileOpenApp("Main"));
            }}
          >
            Back
          </IonButton>
        );
      case "Back":
        return (
          <IonButton
            color="danger"
            onClick={() => {
              dispatch(actionBudgetGoBack());
            }}
          >
            Back
          </IonButton>
        );
      case "SelectedBudgetPage":
        return (
          <IonButton
            color="danger"
            onClick={() => {
              dispatch(actionSettingsSetMobileHeaderTitle("Budget"));
              dispatch(actionBudgetGoBack());
            }}
          >
            Back
          </IonButton>
        );
      default:
        break;
    }
  };
  return <>{handleBudgetHeaderBtnContent()}</>;
};

export default BudgetHeaderBtn;
