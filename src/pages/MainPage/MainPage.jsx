import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import { useSelector } from "react-redux";
import MainMenu from "../../components/MainMenu/MainMenu";
import Budget from "../../Apps/Budget/Budget";
import BudgetHeaderBtn from "../../components/budgetHeaderBtn";

const MainPage = () => {
  const mobileHeaderTitle = useSelector(
    (state) => state.settingsReducer.mobileHeaderTitle
  );
  const mobileCurrentApp = useSelector(
    (state) => state.settingsReducer.mobileCurrentApp
  );
  const handleHeaderbtn = () => {
    switch (mobileCurrentApp) {
      case "Budget":
        return <BudgetHeaderBtn />;
      default:
        break;
    }
  };
  const handleMobileContent = () => {
    switch (mobileCurrentApp) {
      case "Budget":
        return <Budget />;
      default:
        return <></>;
    }
  };
  return (
    <IonPage>
      <MainMenu />
      <IonHeader>
        <IonToolbar mode="ios">
          <IonMenuButton slot="start" />
          <IonTitle>{mobileHeaderTitle}</IonTitle>
          <IonButtons slot="end">{handleHeaderbtn()}</IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen id="MainPage">
        {mobileCurrentApp && handleMobileContent()}
      </IonContent>
    </IonPage>
  );
};

export default MainPage;
