import {
  IonButton,
  IonCard,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
} from "@ionic/react";
import React, { useState } from "react";
import dayjs from "dayjs";
import { DatePicker } from "antd";
import { useDispatch } from "react-redux";
import { actionBudgetAddBudget, actionBudgetGoBack } from "../../redux/budgetReducer";
const AddBudgetPage = () => {
  const dispatch = useDispatch(null)
  const [selectedDate, setSelectedDate] = useState(dayjs());
  const handleAddBudgetform = (e) => {
    e.preventDefault();
    let Date = "";
    if (e.target[2].value.Date === undefined) {
      Date = dayjs(selectedDate).format("YYYY-MM-DD");
    } else {
      Date = dayjs(e.target[2].value.Date).format("YYYY-MM-DD");
    }
    dispatch(actionBudgetAddBudget({
      budgetName : e.target[0].value,
      startAmount : e.target[1].value,
      startDate : e.target[2].value
    }));
    dispatch(actionBudgetGoBack());
  };
  return (
    <>
      <IonCard>
        <IonList>
          <form onSubmit={handleAddBudgetform}>
            <IonItem>
              <IonLabel>Budget Name :</IonLabel>
              <IonInput type="text" required />
            </IonItem>
            <IonItem>
              <IonLabel>Amount :</IonLabel>
              <IonInput type="number" step="0.01" required />
            </IonItem>
            <IonItem>
              <IonLabel>Date :</IonLabel>
                <DatePicker
                  defaultValue={selectedDate}
                  onChange={(date) => setSelectedDate(date)}
                />
              
            </IonItem>
            <div className="tc">
              <IonButton size="small" expand="block" className="pt2 pl4 pr4 pb2" type='submit'>Add</IonButton>
            </div>
          </form>
        </IonList>
      </IonCard>
    </>
  );
};

export default AddBudgetPage;
