import React from "react";
import { useSelector } from "react-redux";
import MainBudgetPage from "./MainBudgetPage";
import AddBudgetPage from "./AddBudgetPage";
import SelectedBudgetPage from "./SelectedBudgetPage";
const Budget = () => {
   const budgetCurrentPage = useSelector(state => state.budgetReducer.budgetCurrentPage);
   const handleBudgetPagesContent = () => {
    switch (budgetCurrentPage) {
        case 'MainBudgetPage':
            return <MainBudgetPage />
        case 'AddBudgetPage' : 
          return <AddBudgetPage />
        case 'SelectedBudgetPage' :
          return <SelectedBudgetPage />
        default:
            <></>;
    }
   }
  return (
    <>
        {handleBudgetPagesContent()}
    </>
  );
};

export default Budget;
