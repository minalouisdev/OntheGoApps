import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonItem,
  IonLabel,
  IonList,
} from "@ionic/react";
import React from "react";
import { useSelector } from "react-redux";
const SelectedBudgetPage = () => {
  const selectedBudgetIndex = useSelector(
    (state) => state.budgetReducer.selectedBudgetIndex
  );
  const budgetData = useSelector(
    (state) => state.budgetReducer.budgetsDataArr[selectedBudgetIndex].data
  );
  const budgetInfo = useSelector(
    (state) => state.budgetReducer.budgetsArr[selectedBudgetIndex]
  );
  return (
    <>
      <IonButton
        expand="block"
        size="small"
        className="shodow-2 br3 mt2 mb2 ml4 mr4"
        color="success"
      >
        Add Income
      </IonButton>
      <IonButton
        expand="block"
        size="small"
        color="danger"
        className="shadow-2 br3 mb2 ml4 mr4"
      >
        Add Expens
      </IonButton>
      <IonCard>
        <IonCardHeader mode="ios">
          Total Budget : {budgetInfo.totalBudgetAmount}
        </IonCardHeader>
        <IonCardContent>
          <IonList>
            {budgetData.map((item, index) => {
              return (
                <IonItem lines="full">
                  <IonLabel slot="start">{index === 0 ? 'Start Amount' : 'label'}</IonLabel>
                  <IonLabel slot="end">amount</IonLabel>
                </IonItem>
              );
            })}
          </IonList>
        </IonCardContent>
      </IonCard>
    </>
  );
};

export default SelectedBudgetPage;
