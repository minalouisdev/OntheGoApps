import React from "react";
import { IonButton, IonCard, IonLabel, IonList, IonItem } from "@ionic/react";
import { useSelector, useDispatch } from "react-redux";
import { actionBudgetOpenPage, actionBudgetSetSelectedBudgetIndex } from "../../redux/budgetReducer";
import {actionSettingsSetMobileHeaderTitle} from '../../redux/settingsReducer';
const MainBudgetPage = () => {
  const dispatch = useDispatch(null);
  const budgetsArr = useSelector((state) => state.budgetReducer.budgetsArr);
  return (
    <>
      <IonButton
        size="small"
        expand="block"
        className="shadow-2 br3 mt3 ml4 mr4 mb2"
        onClick={() => {
          dispatch(actionBudgetOpenPage('AddBudgetPage'))
        }}
      >
        Add Budget
      </IonButton>
      {budgetsArr.length ? (
        <IonCard>
          <IonList>
            {budgetsArr.map((item,index) => {
              return (
                <IonItem key={item.budgetName} lines="full" onClick={() => {
                  dispatch(actionBudgetSetSelectedBudgetIndex(index));
                  dispatch(actionSettingsSetMobileHeaderTitle(item.budgetName));
                  dispatch(actionBudgetOpenPage('SelectedBudgetPage'));
                }}>
                  <IonLabel slot="start">{item.budgetName}</IonLabel>
                  <IonLabel slot="end" color={item.totalBudgetAmount >= 0 ? 'success' : 'danger'}>{item.totalBudgetAmount}</IonLabel>
                </IonItem>
              );
            })}
          </IonList>
        </IonCard>
      ) : (
        <></>
      )}
    </>
  );
};

export default MainBudgetPage;
