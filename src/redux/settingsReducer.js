import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    mode : 'mobile',
    mobileCurrentApp : 'Main',
    mobileHeaderTitle : 'OnTheGoApps',
    // openedAppHistory : [],
    // appsCounter : {
    //     Budget : 0
    // }
}

const settingsReducer = createSlice({
    name : 'settingsReducer',
    initialState,
    reducers : {
        actionSettingsSetMobileHeaderTitle : (state,action) => {
            /**
             * payload string with header
             */
            state.mobileHeaderTitle = action.payload
        },
        actionSettingsMobileOpenApp : (state,action) => {
            /**
             * payload string app name
             */
            state.mobileCurrentApp = action.payload
        },
    }
})

export const {
    actionSettingsMobileOpenApp,
    actionSettingsSetMobileHeaderTitle
} = settingsReducer.actions;
export default settingsReducer.reducer;