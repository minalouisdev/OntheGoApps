import { createSlice } from "@reduxjs/toolkit";

/**
 * budgetCurrentPage : string,
 * budgetPagesHistory : [pages strings]
 * selectedBudgetIndex :
 * budgetHeaderBtn : 'Close',
 * budgetsArr : [
 *  {
 *      budgetName : string,
 *      totalBudgetAmount : number
 *  }
 * ]
 * budgetsDataArr : [
 *  {
 *      data : [
 *                  {
 *                      type : string (expens or income),
 *                      amount : number
 *                      date : string format YYYY-MM-DD,
 *                      label : string
 *                  }
 *              ]
 *      }
 * ]
 */

const initialState = {
  budgetCurrentPage: "MainBudgetPage",
  budgetPagesHistory: ["MainBudgetPage"],
  budgetHeaderBtn: "Close",
  budgetsArr: [],
  budgetsDataArr: [],
  selectedBudgetIndex: null,
};

const budgetReducer = createSlice({
  name: "budgetReducer",
  initialState,
  reducers: {
    actionBudgetOpenPage: (state, action) => {
      // args string page name
      state.budgetCurrentPage = action.payload;
      state.budgetPagesHistory.push(action.payload);
      if (action.payload === "SelectedBudgetPage") {
        state.budgetHeaderBtn = "SelectedBudgetPage";
      } else {
        state.budgetHeaderBtn = "Back";
      }
    },
    actionBudgetGoBack: (state, action) => {
      if (state.budgetCurrentPage === "SelectedBudgetPage") {
        state.selectedBudgetIndex = null;
      }
      state.budgetCurrentPage =
        state.budgetPagesHistory[state.budgetPagesHistory.length - 2];
      state.budgetPagesHistory.pop();
      if (state.budgetCurrentPage === "MainBudgetPage") {
        state.budgetHeaderBtn = "Close";
      }
    },
    actionBudgetAddBudget: (state, action) => {
      /**
       * payload : {
       *  startAmount : number,
       *  startDate : date,
       *  budgetName : string
       * }
       */
      state.budgetsArr.push({
        budgetName: action.payload.budgetName,
        totalBudgetAmount: action.payload.startAmount,
      });
      state.budgetsDataArr.push({
        data: [
          {
            type: "income",
            amount: action.payload.startAmount,
            date: action.payload.startDate,
            label: "Start Amount",
          },
        ],
      });
    },
    actionBudgetSetSelectedBudgetIndex: (state, action) => {
      /**
       * payload budget Index
       */
      state.selectedBudgetIndex = action.payload;
    },
  },
});

export const {
  actionBudgetAddBudget,
  actionBudgetOpenPage,
  actionBudgetGoBack,
  actionBudgetSetSelectedBudgetIndex,
} = budgetReducer.actions;

export default budgetReducer.reducer;
