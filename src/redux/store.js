import { combineReducers } from "redux";
import storage from 'redux-persist/lib/storage';
import { persistReducer } from "redux-persist";
import { configureStore } from "@reduxjs/toolkit";
import settingsReducer from "./settingsReducer";
import budgetReducer from "./budgetReducer";
const reducers = combineReducers({
    settingsReducer,
    budgetReducer
})

const persistConfig = {
    key : "root",
    storage,
    whitelist : ['settingsReducer','budgetReducer']
}

const persistedReducer = persistReducer(persistConfig,reducers);

export const store = configureStore({
    reducer : persistedReducer,
    devTools : true
})